constexpr        :会在编译期计算出来，修饰函数
lambda:
是98、03版带有operator()的局部仿函数
当创建lambda函数的时候，编译器内部会生成这样一个仿函数，并从其你作用域中取得参数传递给lambda函数

#############################################################################################
__func__        :只能在函数内部使用，参数不行
#pragma once = _Pragma("once")
区别是，一个是宏，一个是操作符（预编译期也可使用）
#define PR(...) printf(__VA_ARGS__)
__LINE__,__FILE__只在宏函数中起作用，在普通函数中不起作用
__cplusplus 宏已定义成一个具体数字，比如：201103L
long long : printf 时 lld llu
编译期：static_assert()，需要的是常量参数，编译期诊断。

noexcept()        默认noexcept(true)，没有声明noexcept的函数，都是默认声明noexcept(true) 的
template <class T>
void fun() noexcept(noexcept(T())) {}
第二个noexcept是运算符

成员在定义时就初始化，相当于加在构造函数退出前初始化
sizeof 可以计算类的非静态成员了
sizeof(A::a) or sizeof(a.a)
sizeof...(T):计算变参模板参数个数，T为参数类型。

friend 可以省去class关键字，也可以应用于被 typedef 过的别名了，也可以应用于模板参数class P;
template <typename T> class People {
friend T;
};

=default
=delete
final:使用virtual接口无法子类重写，修饰类或者成员函数，后置。
override 是必须重写，后置
模板默认参数：
template<typename T = int >
    class Test{};

函数模板默认参数的位置不受限制，模板类默认参数参照函数的默认参数的规则
默认模板参数通常是需要跟默认函数参数一起使用的

外部模板：作用参考extern 一个变量，用法也一样，即声明到一个头文件中
为了减少编译时间，即明确表明不需要实例化。
extern template void test<int>();
显式实例化：
template void fun<int>(int);
#############################################################################################
类继承时，子类想使用可以using ,这种做法叫透传，透传也可以用在子类的构造函数上，而且只用一个using就可以
using A::A; // 继承构造函数
透传多个重载的构造函数

using = typedef，类似于赋值语句，起别名的另一种方式。
委托构造：需要一个通用的初始化构造函数

构造函数可能调用其他重载的构造函数：这种做法叫委派，这时的委派构造函数就相当于一个普通函数
test(int i)
try:m_i(i)
{
}
catch(...)
{}

左值：有名称，可取地址
将亡值：std::move后的
T&&: 右值的引用
T&:  左值的引用
右值：不可取地址，不可赋值
改变生存期
移动

std::move 基本等同于一个类型转换：static_cast<T&&>(lvalue);
通常就是一个static_cast

被转化的左值，其生命期并没有随着左右值的转化而改变
移动构造，注意原来的实例已经不能再用。
应该尽量编写不抛出异常的移动构造函数，通过为其添加一个noexcept
关键字，可以保证移动构造函数中抛出来的异常会直接调用terminate 程序终止运行，而不是
造成指针悬挂的状态

所谓完美转发（perfect forwarding），是指在函数模板中，完全依照模板的参数的类型，
将参数传递给函数模板中调用的另外一个函数。
转发：引用折叠（作为函数参数）
TR 的类型定义    声明v 的类型     v 的实际类型
T&              TR              A&
T&              TR&             A&
T&              TR&&            A&
T&&             TR              A&&
T&&             TR&             A&
T&&             TR&&            A&&
定义中出现了左值引用，引用折叠总是优先将其折叠为左值引用
在类方面，多了移动构造函数和移动赋值函数

显示转换函数:
explicit operator bool() const { ... }

{}初始化一切都可以了

POD：兼容C,class要是平凡的;class对象的布局要是标准的。
平凡的:无自定义构造/析构函数，无赋值，拷贝构造函数及其移动版;无虚函数。
标准的:所有的非静态成员函数具有相同的访问权限;多重继承没有导致内存布局上变化的。

自定义字面量后缀。
bool operator "" _C()
auto a = 11_C;

inline namespace
using 模板别名：using aa=vector<int>
#############################################################################################
改进>
auto
auto可以用在宏中。
auto 非静态成员变量，auto 函数参数，auto 数组 不行。
auto 知道类型想简写，decltype 知道类型想使用此类型。

decltype 使用于范型，decltype有推导规则,只适用于表达式，不适用于函数名。
decltype 会使const 或 valotile 失效，即成员不会继承对象的这些属性。

auto 一般修饰变量或者函数返回值，decltype 可以使用于任何需要计算类型的地方，其参数可以是变量名或者表达式。

追踪函数的返回类型:->type
for(auto e:...)        /// 不修改元素内容
for(auto& e:...)    /// 修改元素内容
for(const auto& e:...)    gcc 编译也没错
#############################################################################################
enum class/struct Type{}:强类型枚举值（不允许隐式转换）
delete [] nullptr:ok
unique_ptr,shared_ptr参见参考右值特性来使用，weak_ptr，只有引用但没有计数，与shared_ptr合用
#############################################################################################
编译时常量：constexpr,const 运行时

变长模板：
template <typename ... element> class tuple;
element:模板参数包
实现：递归继承（模板类继承），有三个步骤
1，普通变长模板声明
2，递归定义
3，递归结束：
template<>
    已定义的模板类名字
    {
        用于继承的数据成员
    }
变长模板函数的话，内部也要递归实现
sizeof...()，计算变长参数个数

template<typename ... A> class T: public B(A)...{};
template<typename ... A> class T: public B(A...){};
扩展是不一样的。

多线程
cstdlib:atexit    (C的退出注册函数)
c++11,at_quick_exit,at_exit(正常退出)
terminate,abort:异常退出

#############################################################################################
默认函数操作符
operator,
operator&
operator&&
operator*
operator->
operator->*
operator new
operator delete
声明了构造函数，导致非POD
=default       使用默认
=deleted       禁用

lambda:    // 默认是一个const 函数
[]()mutable->return-type
{}
[var]  按值传递单个变量
[=]，按值传递所有父作用域的变量（包括this）
[&var]
[&]        按引用传递所有父作用域的变量（包括this）
[]:不使用外部变量
[this]:可以使用相关成员变量及函数

可以把lambda函数赋值给一个auto 变量，在以后当成函数指针来使用。

以上变量，只能是自动变量，即不能是全局及静态变量
以上可以混用
本质是一个类，重载了 operator() ，可以带参数，A a; a();
按值传递时，变量值只计算一次，按引用时，在调用时计算



by_ref_lambda        ref;
by_val_lambda        const;
alignof：查看对齐
alginas：设定对齐

c++原生字符串
R"hello" = R"(hello)",总之，可以把 R"()"当成一个函数来使用。()前后可以加前后缀，但不影响()内的内容。
R可与以下字符前缀一起使用。

字符串前缀（表示编码方式）
u8  u  u32    (u 表示 unicode)  L(表示w_char)
\u4个16进制字符   表示某个UTF-16字符
\U8个16进制字符   表示某个UTF-32字符


extern,static,thread_local
thread_local变量，在某个线程中使用时，是独立的，即不影响其他线程对此变量的访问及修改。
在线程启动时构造，在线程结束时析构。

添加long long类型，至少64位。添加 ull 等后缀。

增加了0b,0B前缀，用来表示二进制数据







