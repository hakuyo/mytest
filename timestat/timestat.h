#ifndef TIMESTAT_INCLUDE_H
#define TIMESTAT_INCLUDE_H

#include <time.h>
#include <sys/time.h>
#include <string>
#include <map>

using std::map;
using std::string;

enum IntervalUnit { UNIT_SECOND,UNIT_USECOND};

class CTimeStat
{
public:
    // 添加统计步骤
    void AddStep(const string& step);
    timeval GetStep(const string& step);
    // 获取步骤间的时间间隔，如果只有一个步骤，返回0
    // step1 - step2
    double GetInterval(const string& step1,const string& step2, IntervalUnit unit = UNIT_SECOND);

    static double TimeDiff(const struct timeval& tv1,
            const struct timeval& tv2, IntervalUnit unit = UNIT_SECOND);
private:
    map<string,timeval>     m_times;
};




#endif








