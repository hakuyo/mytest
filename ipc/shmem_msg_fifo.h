#ifndef SHMEM_QUEUE_H_INCLUDE
#define SHMEM_QUEUE_H_INCLUDE

#include <string>
#include <vector>

#include "shmem.h"
#include "sem_posix.h"

using std::string;
using std::vector;

struct MemMsgHeader
{
    // 记录已使用的空间
    unsigned int    m_pushedsize; 
    // 记录已被取出的空间
    unsigned int    m_popedsize; 
    // 消息个数
    unsigned int    m_msgnum;
    // 消息结束位置，即需要从头开始 push 时，所有 msg 所占用的最大共享内存
    // pop 到最后一条消息时，需要注意
    unsigned int    m_endsize;

    MemMsgHeader():
        m_pushedsize(0)
        ,m_popedsize(0)
        ,m_msgnum(0)
        ,m_endsize(0)
    {}
};

// 先入先出
class CShmemMsgFifo
{
public:
    CShmemMsgFifo(CShmem* pshmem,CSem* psem);

    // 把以msg起始地址的 len 个字节存入 pshmem
    bool push(void* msg,unsigned int len);
    // 获取存入pshmem 中的某个消息,*buf 需要 free(不是delete) ,返回*buf的长度
    unsigned int pop(char** buf);

private:
    // 空间总大小
    unsigned int    m_size;
    CShmem          *m_shmem;
    CSem            *m_sem;
};




#endif










