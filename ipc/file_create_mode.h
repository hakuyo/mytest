#ifndef FILE_CREATE_MODE_H_INCLUDE
#define FILE_CREATE_MODE_H_INCLUDE
enum FILE_CREATE_MODE
{
    USER_RWX    = S_IRWXU,
    USER_R      = S_IRUSR,
    USER_W      = S_IWUSR,
    USER_X      = S_IXUSR,
    GROUP_RWX   = S_IRWXG,
    GROUP_R     = S_IRGRP,
    GROUP_W     = S_IWGRP,
    GROUP_X     = S_IXGRP,
    OTHER_RWX   = S_IRWXO,
    OTHER_R     = S_IROTH,
    OTHER_W     = S_IWOTH,
    OTHER_X     = S_IXOTH,
    SETUERSID   = S_ISUID,
    SETGROUPID  = S_ISGID,
    MODE_END    = S_ISVTX,
};

#endif



