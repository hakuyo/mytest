#ifndef SHMEM_H_INCLUDE
#define SHMEM_H_INCLUDE

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <string>
#include "file_create_mode.h"

using std::string;
// link with -lrt

enum SH_RW
{
    SH_RDONLY   = O_RDONLY,
    SH_RDWR     = O_RDWR,
    SH_CREAT    = O_CREAT,
    SH_EXCL     = O_EXCL
};

class CShmem
{
public:
    CShmem(const string& name,unsigned int size = 1024,
            int flag = SH_RDWR | SH_CREAT,
            int mode = USER_R|USER_W);
    ~CShmem();

    bool open();

    bool isOpened();
    
    void* get()
    {
        return m_addr;
    }
    
    unsigned int size()
    {
        return m_size;
    }
private:
    CShmem(const CShmem& other);
    CShmem& operator=( const CShmem& other);

    bool map(bool bExist);
    void unmap();
    void unlink();
private:
    string  m_name;
    int     m_fd;
    int     m_flags;
    int     m_mode;
    unsigned int m_size;
    void*   m_addr;
};




#endif










