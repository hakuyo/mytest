#!/usr/bin/sh

usage()
{
    echo "usage:"
    echo -e "\t./undone.sh business.d-path"
    exit 0
}

if [[ $# != 1 ]]; then
    usage
fi

if [[ ! -d $1 ]]; then
    echo "$1 not exist"
    exit 0
fi

bdir=${1%/}
bfile=$(grep -n '^-\sID' ${bdir}/business.d/*.b)
# 保留newline，变量需要加双引号
business=$(echo "${bfile}" | awk '{print $3}')

# url的格式： ptjy/dzqy/xycx
for url in ${business}; do
    # ggt_tyzh ggt_ptyw ptjy/dzqy 不移植
    if [[ ${url:0:3} == "ggt" ]]; then
        continue
    elif [[ ${url:0:9} == "ptjy/dzqy" ]]; then
        continue
    fi

    dirpath=$(echo ${url} | awk -F / '{printf "%s_%s", $1,$2}')
    #if [[ ! -d ${dirpath} ]]; then
    #    echo "${dirpath} directory not exist"
    #    continue
    #fi
    #echo "${dirpath}"
    fpath=${dirpath}/${url//\//_}.cpp
    #echo "${fpath}"
    if [[ ! -f ${fpath} ]]; then
        echo "${fpath} file not exist"
        continue
    fi
done








