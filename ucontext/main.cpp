#include <iostream>
#include <ucontext.h>
#include <unistd.h>
#include <pthread.h>
#include "uccontext.h"

using namespace std;

void func1(void* old);
void func2(void* old);

UContext *p1 = new UContext(1024*64,func1,NULL);
UContext *p2 = new UContext(1024*64,func2,NULL);
int test_count = 0;

int main(int argc,char *argv[])
{
    if (argc < 2) {
        cout<<argv[0] <<" times"<<endl;
        return -2;
    }

    test_count      = atoi(argv[1]);

    int test_count_2 = (test_count + 1) * 2;
    int test_count_3 = (test_count + 1);
    for (int i = 0; i < test_count_2; i++) {
        if (i < test_count_3) {
            p1->resume();
        } else {
            p2->resume();
        }
    }

    delete p1;
    delete p2;
    return 0;
}

void func1(void* old)
{
    for (int i = 0; i < test_count; i++) {
        cout<<"func1 yielding ..."<<endl;
        p1->yield();
    }
    cout<<"func1 end"<<endl;
}

void func2(void* old)
{
    for (int i = 0; i < test_count; i++) {
        cout<<"func2 yielding ..."<<endl;
        p2->yield();
    }
    cout<<"func2 end"<<endl;
}


