#include <iostream>
using namespace std;
int main(int argc, char** argv)
{
	int i = 1;
	int b = i++ + ++i + i;
	cout<<"b:"<<b<<";i:"<<i<<endl;
	i = 13;
	b = i-- - --i;
	cout<<"b:"<<b<<";i:"<<i<<endl;
	i = 1;
	b = i++ *  ++i * i ;
	cout<<"b:"<<b<<";i:"<<i<<endl;
	return 0;
}
/*
b:7;i:3
b:2;i:11
b:9;i:3

注意操作符顺序即可。
*/