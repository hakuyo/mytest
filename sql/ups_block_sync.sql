
---#####################################################################--
use [NEWS]
go

create function getMarket(@intMarket int)
returns varchar(2)
as
begin
	declare @market varchar(2)
	SET @market=''
	if (@intMarket = 0)
	begin
		set @market = 'SZ'
	end
	else if (@intMarket = 1)
	begin
		set @market = 'SH'
	end
	return @market
end
go

-- tableType=1 查询common，=2查询bigdata
create function getMaxUpdatetime(@tableType int ,@blockType int=0 )
returns bigint
as
begin
	declare @result bigint
	if (@tableType=1)
	begin
		select @result = max(plate.UPDATETIME) from NEWS_TY.dbo.plate_name as plate where plate.[TYPE] = @blockType
	end
	if (@tableType=2)
	begin
		select @result =max(plate.UPDATETIME) from NEWS_TY.dbo.BDC_CONC as plate
	END
	IF (@result IS null)
	BEGIN
		SET @result=0
	END
	return @result
end
go

create procedure dbo.usp_block_sync_common
as 
begin 

SET NOCOUNT ON
-- #########################
-- 填充 TBL_BLK_STOCK
/*
CREATE TABLE [dbo].[TBL_BLK_STOCK](
	[STOCK_ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[STOCK_CODE] [varchar](50) NOT NULL,
	[STOCK_NAME] [varchar](50) NOT NULL,
	[BLOCK_CODE] [varchar](50) NOT NULL,
	[STOCK_PY] [varchar](20) NOT NULL,
	[LISTING_DATE] [datetime] NULL,
	[MARKET_CODE] [varchar](10) NOT NULL,
	[IS_VALID] [int] NOT NULL,
	[IS_NEW] [int] NOT NULL,
	[MEMO] [varchar](300) NOT NULL,
	[INNERCODE] [varchar](50) NULL,
	[CREATETIME] [datetime] NOT NULL,
	[LASTTIME] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_BLK_STOCK] PRIMARY KEY CLUSTERED 
(
	[STOCK_CODE] ASC,
	[BLOCK_CODE]
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
*/
BEGIN TRANSACTION UPS_COMMON_TRAN
BEGIN TRY

DECLARE @recNumStock BIGINT
DECLARE @recNumBlock BIGINT

SELECT @recNumStock = COUNT(*) FROM NEWS_TY.dbo.plate_stock
SELECT @recNumBlock = COUNT(*) FROM NEWS_TY.dbo.plate_name

IF (@recNumStock = 0 OR @recNumBlock = 0)
BEGIN
	GOTO END_COMMIT
END

delete from TBL_BLK_STOCK
DBCC CHECKIDENT (TBL_BLK_STOCK, RESEED,1)

declare @stockPy varchar(20)
declare @listingDate datetime
declare @isNew int
declare @memo varchar(300)

set @stockPy=''
set @listingDate=NULL
set @isNew=0
set @memo=''
-- 这里的tockcode 有市场前缀

merge TBL_BLK_STOCK as targetT
using (select distinct 
pstock.IDD as scode,
pstock.STK_SHORT_NAME as nm,
@stockPy as py,
@listingDate as listingdate,
pstock.SEC_MAR_PAR as marketcode,
pstock.ISVALID as isvalid,
@isNew as isnew,
@memo as memo,
pstock.ID as innercode,
DATEADD(HOUR,8,DATEADD(SECOND,CONVERT(int,pstock.UPDATETIME/1000),'1970-01-01')) as uptime,
pstock.PLATE_CODE as blockid
from NEWS_TY.dbo.plate_stock as pstock 
inner join NEWS_TY.dbo.plate_name as plate on plate.ID = pstock.PLATE_CODE ) as srcT
on targetT.STOCK_CODE = dbo.getMarket(srcT.marketcode)+srcT.scode and targetT.BLOCK_CODE=srcT.blockid
when not matched
then insert(STOCK_CODE,STOCK_NAME,BLOCK_CODE,STOCK_PY,LISTING_DATE,MARKET_CODE,IS_VALID,IS_NEW,MEMO,INNERCODE,CREATETIME,LASTTIME) 
	 values(dbo.getMarket(srcT.marketcode)+srcT.scode,srcT.nm,CONVERT(varchar(16),srcT.blockid),srcT.py,srcT.listingdate,dbo.getMarket(srcT.marketcode),srcT.isvalid,
	 srcT.isnew,srcT.memo,srcT.innercode,srcT.uptime,srcT.uptime);
	
-- #########################
-- 填充 TBL_BLK_BLOCK
/*
CREATE TABLE [dbo].[TBL_BLK_BLOCK](
	[BLOCK_ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[BLOCK_NAME] [varchar](100) NOT NULL,
	[BLOCK_CODE] [varchar](50) NOT NULL,
	[IS_VALID] [int] NOT NULL,
	[CATEGORY_CODE] [varchar](50) NULL,
	[MEMO] [varchar](300) NOT NULL,
	[CREATETIME] [datetime] NOT NULL,
	[LASTTIME] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_BLK_BLOCK] PRIMARY KEY CLUSTERED 
(
	[BLOCK_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
*/

set @memo=''

delete from TBL_BLK_BLOCK
DBCC CHECKIDENT (TBL_BLK_BLOCK, RESEED,1)

merge TBL_BLK_BLOCK as targetT
using (select distinct plate.PLATE_NAME as nm,plate.ID as id,plate.ISVALID as isvalid, plate.[TYPE] as ptype, @memo as memo, 
DATEADD(HOUR,8,DATEADD(SECOND,CONVERT(int,plate.UPDATETIME/1000),'1970-01-01')) as uptime from NEWS_TY.dbo.plate_name as plate) as srcT
on targetT.BLOCK_NAME = srcT.nm and targetT.BLOCK_CODE=srcT.id and targetT.CATEGORY_CODE = CONVERT(int,srcT.ptype)
when not matched 
then insert(BLOCK_NAME,BLOCK_CODE,CATEGORY_CODE,IS_VALID,MEMO,CREATETIME,LASTTIME) 
	values(srcT.nm,srcT.id,CONVERT(int,srcT.ptype),CONVERT(int,srcT.isvalid),srcT.memo,srcT.uptime,srcT.uptime);
	
-- #########################
-- 填充  TBL_BLK_CATEGORY
/*
CREATE TABLE [dbo].[TBL_BLK_CATEGORY](
	[CATEGORY_ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CATEGORY_NAME] [varchar](100) NOT NULL,
	[CATEGORY_CODE] [varchar](50) NOT NULL,
	[ORDER_NUM] [int] NOT NULL,
	[IS_VALID] [int] NOT NULL,
	[MEMO] [varchar](300) NOT NULL,
	[CREATETIME] [datetime] NOT NULL,
	[LASTTIME] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_BLK_CATEGORY] PRIMARY KEY CLUSTERED 
(
	[CATEGORY_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
*/
set @memo=''
declare @plateName varchar(64)
declare @isValid int
declare @catgory int

set @plateName=N'行业板块'
set @isValid=1
set @catgory=1

delete from TBL_BLK_CATEGORY
DBCC CHECKIDENT (TBL_BLK_CATEGORY, RESEED,0)

merge TBL_BLK_CATEGORY as targetT
using (select distinct @catgory as ptype,@isValid as isvalid,@memo as memo,@plateName as pName,
DATEADD(HOUR,8,DATEADD(SECOND,CONVERT(int,dbo.getMaxUpdatetime(1,1)/1000),'1970-01-01')) as uptime) as srcT
on targetT.CATEGORY_CODE = srcT.ptype
when not matched
then insert(CATEGORY_NAME,CATEGORY_CODE,ORDER_NUM,IS_VALID,MEMO,CREATETIME,LASTTIME)
	 values( srcT.pName,srcT.ptype,srcT.ptype,srcT.isvalid,srcT.memo,srcT.uptime,srcT.uptime);

set @plateName=N'地域板块'
set @isValid=1
set @catgory=2
merge TBL_BLK_CATEGORY as targetT
using (select distinct @catgory as ptype,@isValid as isvalid,@memo as memo,@plateName as pName,
DATEADD(HOUR,8,DATEADD(SECOND,CONVERT(int,dbo.getMaxUpdatetime(1,2)/1000),'1970-01-01')) as uptime) as srcT
on targetT.CATEGORY_CODE = srcT.ptype
when not matched
then insert(CATEGORY_NAME,CATEGORY_CODE,ORDER_NUM,IS_VALID,MEMO,CREATETIME,LASTTIME)
	 values( srcT.pName,srcT.ptype,srcT.ptype,srcT.isvalid,srcT.memo,srcT.uptime,srcT.uptime);
	 
set @plateName=N'概念板块'
set @isValid=1
set @catgory=3
merge TBL_BLK_CATEGORY as targetT
using (select distinct @catgory as ptype,@isValid as isvalid,@memo as memo,@plateName as pName,
DATEADD(HOUR,8,DATEADD(SECOND,CONVERT(int,dbo.getMaxUpdatetime(1,3)/1000),'1970-01-01')) as uptime) as srcT
on targetT.CATEGORY_CODE = srcT.ptype
when not matched
then insert(CATEGORY_NAME,CATEGORY_CODE,ORDER_NUM,IS_VALID,MEMO,CREATETIME,LASTTIME)
	 values( srcT.pName,srcT.ptype,srcT.ptype,srcT.isvalid,srcT.memo,srcT.uptime,srcT.uptime);

GOTO END_COMMIT
END TRY

BEGIN CATCH
	GOTO END_ROLLBACK
END CATCH

END_COMMIT:
COMMIT TRANSACTION UPS_COMMON_TRAN
GOTO END_PROC

END_ROLLBACK:
ROLLBACK TRANSACTION UPS_COMMON_TRAN

END_PROC:

end
go

--############## 同步 bigdata ###################
create procedure dbo.usp_block_sync_bigdata
as 
begin
SET NOCOUNT ON

BEGIN TRANSACTION UPS_BIGDATA_TRAN
BEGIN TRY

DECLARE @recNumStock BIGINT
DECLARE @recNumBlock BIGINT

SELECT @recNumStock = COUNT(*) FROM NEWS_TY.dbo.BDC_CONC_STOCK
SELECT @recNumBlock = COUNT(*) FROM NEWS_TY.dbo.BDC_CONC

IF (@recNumStock = 0 OR @recNumBlock = 0)
BEGIN
	GOTO END_COMMIT
END

declare @stockPy varchar(20)
declare @listingDate datetime
declare @isNew int
declare @memo varchar(200)

set @stockPy=''
set @listingDate=NULL
set @isNew=0
set @memo=''

-- 这里的tockcode 有市场前缀
merge TBL_BLK_STOCK as targetT
using (select distinct
tstock.STOCKCODE as scode,
tstock.[NAME] as nm,
@stockPy as py,
@listingDate as listingdate,
tstock.MARKETCODE as marketcode,
tstock.ISVALID as isvalid,
@isNew as isnew,
@memo as memo,
tstock.STOCKCODE as innercode,
DATEADD(HOUR,8,DATEADD(SECOND,CONVERT(int,tstock.UPDATETIME/1000),'1970-01-01')) as uptime,
constock.CONC_ID as blockid
from NEWS_TY.dbo.BDC_CONC_STOCK as constock
inner join NEWS_TY.dbo.BDC_CONC as plate on plate.ID = constock.CONC_ID
inner join NEWS_TY.dbo.t_stock as tstock on constock.STOCK_CODE = tstock.STOCKCODE) as srcT
on targetT.STOCK_CODE = dbo.getMarket(srcT.marketcode)+srcT.scode and targetT.BLOCK_CODE=srcT.blockid
when not matched
then insert(STOCK_CODE,STOCK_NAME,BLOCK_CODE,STOCK_PY,LISTING_DATE,MARKET_CODE,IS_VALID,IS_NEW,MEMO,INNERCODE,CREATETIME,LASTTIME) 
	 values(dbo.getMarket(srcT.marketcode)+srcT.scode,srcT.nm,CONVERT(varchar(16),srcT.blockid),srcT.py,srcT.listingdate,dbo.getMarket(srcT.marketcode),
	 srcT.isvalid,srcT.isnew,srcT.memo,srcT.innercode,srcT.uptime,srcT.uptime);

	
-- #########################
-- 填充 TBL_BLK_BLOCK

set @memo=''
declare @isValid int,@catgory int
set @isValid=1
set @catgory=4

merge TBL_BLK_BLOCK as targetT
using (select distinct plate.CONCEPT_NAME as nm,plate.ID as id,@isValid as isvalid,@memo as memo, @catgory as ptype,
DATEADD(HOUR,8,DATEADD(SECOND,plate.UPDATETIME/1000,'1970-01-01')) as uptime from NEWS_TY.dbo.BDC_CONC as plate) as srcT
on targetT.BLOCK_NAME = srcT.nm and targetT.BLOCK_CODE=srcT.id and targetT.CATEGORY_CODE = srcT.ptype
when not matched 
then insert(BLOCK_NAME,BLOCK_CODE,CATEGORY_CODE,IS_VALID,MEMO,CREATETIME,LASTTIME) 
	values(srcT.nm,srcT.id,srcT.ptype,srcT.isvalid,srcT.memo,srcT.uptime,srcT.uptime);

-- #########################
-- 填充  TBL_BLK_CATEGORY

declare @plateName varchar(64)
set @catgory=4
set @memo=''
set @isValid=1
set @plateName=N'概念板块'

merge TBL_BLK_CATEGORY as targetT
using (select @plateName as nm,@isValid as isvalid, @memo as memo,
DATEADD(HOUR,8,DATEADD(SECOND,dbo.getMaxUpdatetime(2,0)/1000,'1970-01-01')) as uptime,
@catgory as ptype) as srcT
on targetT.CATEGORY_CODE = srcT.ptype
when not matched
then insert(CATEGORY_NAME,CATEGORY_CODE,ORDER_NUM,IS_VALID,MEMO,CREATETIME,LASTTIME)
	 values(srcT.nm,srcT.ptype,srcT.ptype,srcT.isvalid,srcT.memo,srcT.uptime,srcT.uptime);
---#####################################################################--
GOTO END_COMMIT
END TRY

BEGIN CATCH
	GOTO END_ROLLBACK
END CATCH

END_COMMIT:
COMMIT TRANSACTION UPS_BIGDATA_TRAN
GOTO END_PROC

END_ROLLBACK:
ROLLBACK TRANSACTION UPS_BIGDATA_TRAN

END_PROC:
end
go

---##############   block 文件存储过程  ##############################

USE [NEWS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[USP_BLK_BLOCK_LIST]
AS
BEGIN
	SET NOCOUNT ON
	select distinct a.BLOCK_NAME ,b.CATEGORY_NAME from TBL_BLK_block a
    inner join tbl_blk_category b on a.IS_VALID = 1 and b.is_valid = 1 
	and  a.CATEGORY_CODE = b.CATEGORY_CODE and a.CATEGORY_CODE in (1,2,4)
	order by b.CATEGORY_NAME,a.BLOCK_NAME
	
end
go

CREATE PROCEDURE [dbo].[USP_BLK_STOCK_LIST] 
(  @BLOCK_NAME VARCHAR(100) = '' )

AS
BEGIN
SET NOCOUNT ON

IF (@BLOCK_NAME = '')
BEGIN
	select distinct b.stock_code,a.BLOCK_NAME
	from TBL_BLK_block  a
	inner join TBL_BLK_STOCK b on b.BLOCK_CODE = a.block_code
	inner join tbl_blk_category c on a.category_code = c.category_code and c.is_valid = 1
	AND c.CATEGORY_CODE in (1,2,4)
	order by a.BLOCK_NAME, b.stock_code
	
END
ELSE
BEGIN
   select distinct b.stock_code,a.BLOCK_NAME
	from TBL_BLK_block  a
	inner join TBL_BLK_STOCK b on b.BLOCK_CODE = a.block_code
	inner join tbl_blk_category c on a.category_code = c.category_code and c.is_valid = 1
	AND c.CATEGORY_CODE in (1,2,4)
	where a.BLOCK_NAME like '%' + @BLOCK_NAME + '%'
	order by a.BLOCK_NAME, b.stock_code
END
    
end
GO


---##############   生成 news block 文件 表 ############################## 

USE [NEWS]
GO

/****** Object:  Table [dbo].[TBL_BLK_BLOCK]    Script Date: 08/09/2017 17:19:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TBL_BLK_BLOCK](
	[BLOCK_ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[BLOCK_NAME] [varchar](100) NOT NULL,
	[BLOCK_CODE] [varchar](16) NOT NULL,
	[IS_VALID] [int] NOT NULL,
	[CATEGORY_CODE] [varchar](50) NULL,
	[MEMO] [varchar](300) NOT NULL,
	[CREATETIME] [datetime] NOT NULL,
	[LASTTIME] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_BLK_BLOCK] PRIMARY KEY CLUSTERED 
(
	[BLOCK_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[TBL_BLK_BLOCK] ADD  CONSTRAINT [DF_TBL_BLK_BLOCK_IS_VALID]  DEFAULT ((1)) FOR [IS_VALID]
GO

ALTER TABLE [dbo].[TBL_BLK_BLOCK] ADD  CONSTRAINT [DF_TBL_BLK_BLOCK_CREATETIME]  DEFAULT (getdate()) FOR [CREATETIME]
GO

ALTER TABLE [dbo].[TBL_BLK_BLOCK] ADD  CONSTRAINT [DF_TBL_BLK_BLOCK_LASTTIME]  DEFAULT (getdate()) FOR [LASTTIME]
GO


USE [NEWS]
GO

/****** Object:  Table [dbo].[TBL_BLK_CATEGORY]    Script Date: 08/09/2017 17:24:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TBL_BLK_CATEGORY](
	[CATEGORY_ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CATEGORY_NAME] [varchar](100) NOT NULL,
	[CATEGORY_CODE] [varchar](50) NOT NULL,
	[ORDER_NUM] [int] NOT NULL,
	[IS_VALID] [int] NOT NULL,
	[MEMO] [varchar](300) NOT NULL,
	[CREATETIME] [datetime] NOT NULL,
	[LASTTIME] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_BLK_CATEGORY] PRIMARY KEY CLUSTERED 
(
	[CATEGORY_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[TBL_BLK_CATEGORY] ADD  CONSTRAINT [DF_TBL_BLK_CATEGORY_ORDER_NUM]  DEFAULT ((0)) FOR [ORDER_NUM]
GO

ALTER TABLE [dbo].[TBL_BLK_CATEGORY] ADD  CONSTRAINT [DF_TBL_BLK_CATEGORY_IS_VALID]  DEFAULT ((1)) FOR [IS_VALID]
GO

ALTER TABLE [dbo].[TBL_BLK_CATEGORY] ADD  CONSTRAINT [DF_TBL_BLK_CATEGORY_CREATETIME]  DEFAULT (getdate()) FOR [CREATETIME]
GO

ALTER TABLE [dbo].[TBL_BLK_CATEGORY] ADD  CONSTRAINT [DF_TBL_BLK_CATEGORY_LASTTIME]  DEFAULT (getdate()) FOR [LASTTIME]
GO

USE [NEWS]
GO

/****** Object:  Table [dbo].[TBL_BLK_STOCK]    Script Date: 08/09/2017 17:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TBL_BLK_STOCK](
	[STOCK_ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[STOCK_CODE] [varchar](16) NOT NULL,
	[STOCK_NAME] [varchar](50) NOT NULL,
	[BLOCK_CODE] [varchar](16) NOT NULL,
	[STOCK_PY] [varchar](20) NOT NULL,
	[LISTING_DATE] [datetime] NULL,
	[MARKET_CODE] [varchar](10) NOT NULL,
	[IS_VALID] [int] NOT NULL,
	[IS_NEW] [int] NOT NULL,
	[MEMO] [varchar](300) NOT NULL,
	[INNERCODE] [varchar](16) NULL,
	[CREATETIME] [datetime] NOT NULL,
	[LASTTIME] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_BLK_STOCK] PRIMARY KEY CLUSTERED 
(
	[STOCK_CODE] ASC,
	[BLOCK_CODE]
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[TBL_BLK_STOCK] ADD  CONSTRAINT [DF_TBL_BLK_STOCK_IS_VALID]  DEFAULT ((1)) FOR [IS_VALID]
GO

ALTER TABLE [dbo].[TBL_BLK_STOCK] ADD  CONSTRAINT [DF_TBL_BLK_STOCK_IS_NEW]  DEFAULT ((0)) FOR [IS_NEW]
GO

ALTER TABLE [dbo].[TBL_BLK_STOCK] ADD  CONSTRAINT [DF_TBL_BLK_STOCK_CREATETIME]  DEFAULT (getdate()) FOR [CREATETIME]
GO

ALTER TABLE [dbo].[TBL_BLK_STOCK] ADD  CONSTRAINT [DF_TBL_BLK_STOCK_LASTTIME]  DEFAULT (getdate()) FOR [LASTTIME]
GO

















