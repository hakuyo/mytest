#include <iostream>

using namespace std;

struct List
{
    List()
    {
        n       = 0;
        pNext   = NULL;
    }
    int n;
    List* pNext;
};

void reverse(List ** ppList)
{
    if (ppList == NULL)
        return;

    List* pList = *ppList;
    List* pHead = pList;
    List* pTail = NULL;

    bool bFirst = true;
    while(pHead != NULL)
    {
        List* pCur      = pHead;
        if ( bFirst )
        {
            bFirst      = false;
            pHead       = pHead->pNext;
            pCur->pNext = NULL;
        }
        else
        {
            pHead       = pHead->pNext;
            pCur->pNext = pTail;
        }
        pTail           = pCur;
    }
    *ppList = pTail;
}

void printList( List* pList)
{
    while(pList != NULL)
    {
        cout<<pList->n<<":";
        pList = pList->pNext;
    }
    cout<<endl;
}

int main()
{
    List* pList = new List;
    List* pHead = pList;
    for(int i = 0; i < 5; ++i)
    {
        pList->n    = i;
        pList->pNext= new List;
        pList       = pList->pNext;
    }

    printList(pHead);
    reverse(&pHead);
    printList(pHead);
    return 0;
}


