--
--用premake4构建跨平台应用程序要注意以下几点
--1.应用程序所使用的库应该使用跨平台的库
--2.include路径及连接库的设置，在这次测试中没有加入
--3.可以先建相关文件夹，再编写premake4.lua文件
--
-- A solution contains projects, and defines the available configurations
solution "MyApplication"
   configurations { "Debug", "Release" }
--configurations { "DebugLib", "DebugDLL", "ReleaseLib", "ReleaseDLL" }
   local binpathprefixx = "bin/"
   local buildpathprefixx = "build/"
   
   project "MyApplication1"
    kind "ConsoleApp"
    language "C++"
    files { "MyApplication1/Header/**.h", "MyApplication1/**.cpp" }
    location "MyApplication1/"
	
      configuration "Debug"
		objdir (buildpathprefixx .. "MyApplication1/")
		targetdir (binpathprefixx .. "MyApplication1/" .. "Debug/")
        defines { "DEBUG" }
        flags { "Symbols" }
 
      configuration "Release"
		objdir (buildpathprefixx .. "MyApplication1/")
		targetdir (binpathprefixx .. "MyApplication1/" .. "Release/")
         defines { "NDEBUG" }
         flags { "Optimize" }
		 
	project "MyApplication2"
      kind "ConsoleApp"
      language "C++"
      files { "MyApplication2/Header/**.h", "MyApplication2/Source/**.cpp" }
	  location "MyApplication2/"
	  
      configuration "Debug"
		objdir (buildpathprefixx .. "MyApplication2/")
		targetdir (binpathprefixx .. "MyApplication2/" .. "Debug/")
        defines { "DEBUG" }
        flags { "Symbols" }
      configuration "Release"
		objdir (buildpathprefixx .. "MyApplication2/")
		targetdir (binpathprefixx .. "MyApplication2/" .. "Release/")
         defines { "NDEBUG" }
         flags { "Optimize" }
         
         
         
         
         
         
         
         
         
         
         
         
         