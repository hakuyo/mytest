#include <iostream>
using namespace std;

#ifndef __GNUC__
    #error 编译器为g++，windows下，好像没有把##生成的新字符串当成一个宏处理
#endif

#define conn(x,y)    x#y
// x必须是一个字符串，使用时，conn("2",3)，而不能是conn(2,3)，3可以是任何类型的字符串

// ## 有两种用法，一种是字符串连接，另一种是把连接生成的字符串当成一个新的宏
// 新宏必须存在
// 用##连接中参数中包含已定义的宏时，宏不会展开
// ##连接时，要么全是数字，要么，连接生成的字符串必须是已经定义的宏
#define STR test
#define hello(x,y,z)    x##y##z
 
int main()
{
        cout<<conn("2",3)<<endl;         // 输出 23
        // 第一种用法
        cout<<hello(1,2,3)<<endl;        // 输出 123
       // cout<<hello(a,2,3)<<endl;      // 会报错
        // 第二种用法
        #define xyz 333
        cout<<hello(x,y,z)<<endl;        // 即xyz这个宏必须存在，然后输出333
        //cout<<hello(STR,y,z)<endl;     // STR没有展开成test，仍为STRyz
        return 0;
}