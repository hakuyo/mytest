# 目录结构
# .
# ./utils
# 当前目录和./utils目录下都有头文件和源文件
OBJS = $(patsubst %.cpp,%.o,$(wildcard *.cpp))
OBJS += $(patsubst %.cpp,%.o,$(wildcard *.c))
OBJS += $(patsubst %.cpp,%.o,$(wildcard utils/*.cpp))
OBJS += $(patsubst %.cpp,%.o,$(wildcard utils/*.c))

CC_OPTS = -I utils/ -I./
utils/%.o:utils/%.cpp
	$(CC) $(CFLAG) -Wall -c $< $(CC_OPTS) -o $@
utils/%.o:utils/%.c
	$(CC) $(CFLAG) -Wall -c $< $(CC_OPTS) -o $@


#$(filter-out utils/%.o,%.o):%.cpp
%.o:%.cpp
	$(CC) $(CFLAG) -Wall -c $< $(CC_OPTS) 

%.o:%.c
	$(CC) $(CFLAG) -Wall -c $< $(CC_OPTS)


source=$(filter-out %dbutils.cpp,$(wildcard ./*.cpp))

###########################################################
# 单独编译某个 so，找不到目标名通过哪个变量名获取，就添加 make so=so_name的方式
ifdef so
targets = $(so)
objs = $(subst .so,.o,$(so))
else
objs=$(patsubst %.cpp,%.o,$(source))
targets=$(patsubst %.cpp,%.so,$(source))
endif

all:$(targets)

$(targets): $(objs)
	$(CC) $(subst .so,.o,$@) -shared -o $@
###########################################################
一次编译多个文件，省得写 %.o:%.cpp 这种形式
g++ main.cpp hello.cpp -I../include -I./ ../libjson-gcc-4.4.7.a

