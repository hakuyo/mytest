#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
    int pipeFd[2];
	if(pipe(pipeFd) == -1)
	{
		return -1;
	}
	pid_t pid = fork();
//	write(pipeFd[1],"test",4);
	//pid_t pid = fork();
	if(pid == 0)
	{
		close(pipeFd[1]);
		char buf[64] = {'\0'};
		read(pipeFd[0],buf,64);
		printf("read:%s\n",buf);
		close(pipeFd[0]);
	}
        else
        {
		write(pipeFd[1],"test",4);
	}
	//close(pipeFd[1]);
	//close(pipeFd[0]);
	sleep(2);
	return 0;
}