//promise example
#include <iostream>       // std::cout
#include <functional>     // std::ref
#include <thread>         // std::thread
#include <future>         // std::promise, std::future
#include <chrono>

using namespace std;

void hello(const string& msg)
{
    cout<< "worker thread id:" << this_thread::get_id()<<endl;
    cout<<msg<<endl;
}

int main ()
{
    // this_thread::sleep_until 睡到指定时间
    cout<< "hardware_concurrency:" << thread::hardware_concurrency()<<endl;
    cout<< "main thread id:" << this_thread::get_id()<<endl;
    string msg("hello world");
    thread th1 (hello, std::ref(msg));
    
    th1.join();
    return 0;
}
