#ifndef __KDS_MOBILE_STOCK_REDIS_H__
#define __KDS_MOBILE_STOCK_REDIS_H__

#include <string>
#include <vector>
#include <map>
#include "hiredis.h"

using std::string;
using std::pair;
using std::map;

struct IpPort
{
    IpPort(const string& ip, unsigned int port):
        m_ip(ip)
        ,m_port(port)
    {}

    bool operator<(const IpPort& other) const
    {
        if (m_ip == other.m_ip)
            return m_port < other.m_port;

        return m_ip < other.m_ip;
    }

    string          m_ip;
    unsigned int    m_port;
};

class Redis   
{
public:
    Redis();
    ~Redis();
    void SetServer(const string &ip, unsigned int port);

    // key
    void Exec(const string& cmd);
    string GetKey(const string& key);
    void SetKeyValue(const string& key, const string &value, const string &ttl = "1800");
    void HSetKeyValue(const string& hash, const string& key, const string &value, const string &ttl = "1800");
    string HGetKeyValue(const string& hash, const string& key);

    // int
    long long GetInt(const string& key);
    void release();

private:
    void Reconnect();
    map<IpPort,redisContext*>   m_conns;
};
#endif

