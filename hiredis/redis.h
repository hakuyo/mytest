#ifndef __KDS_MOBILE_STOCK_REDIS_H__
#define __KDS_MOBILE_STOCK_REDIS_H__


#include <string>
#include <map>
#include "hiredis.h"
using std::string;
using std::pair;

class Redis 
{
    public:
        Redis(){}
        ~Redis(){}
        static void SetServer(const string &ip, unsigned int port);

        // key
        static void Exec(const string& cmd);
        static string GetKey(const string& cmd);

        static Redis  * instance();
        static void release();
    private:
        bool Reconnect();
        
        static Redis                        *m_instance;
        static redisContext                 *m_ctx;
        static vector<pair<string,unsigned int> >     m_vServers;
};


#endif

