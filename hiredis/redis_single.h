#ifndef _REDIS_H__INCLUDED
#define _REDIS_H__INCLUDED

#include <string>
#include <vector>
#include "hiredis.h"

using std::string;
using std::pair;
using std::vector;

struct RedisConnConf
{
    RedisConnConf(const string& ip, unsigned int port,const string& passwd):
        m_ip(ip)
        ,m_port(port)
        ,m_passwd(passwd)
    {}

    bool operator<(const RedisConnConf& other) const
    {
        if (m_ip != other.m_ip)
            return m_ip < other.m_ip;
    
        return m_port < other.m_port;
    }

    string          m_ip;
    unsigned int    m_port;
    string          m_passwd;
};

class Redis   
{
public:
    Redis(const string &ip, unsigned int port,const string& passwd );
    ~Redis();
    bool Login(redisContext* pconn, const string& passwd);

    // key
    // set时，要set到每一个连接
    // Exec时，如果*presp，外面程序应该手动释放这个指针
    bool Exec(const string& cmd, redisReply **presp);
    void SetKeyValue(const string& key, const string &value, const string &ttl = "");
    void HSetKeyValue(const string& hash, const string& key, const string &value, const string &ttl = "");
    // get时，从一个连接中获取成功时即返回
    string GetKey(const string& key);
    void KeyPattern(const string& key_pat,vector<string>& results);
    string HGetKeyValue(const string& hash, const string& key);
    void HGetValue(const string& table, vector<string> &result);

    // int
    long long GetInt(const string& key);
    void release();
private:
    Redis(const Redis& other);
    Redis& operator=(const Redis& other);
private:
    bool CheckConnStatus(redisContext* pconn, const RedisConnConf& conf);
    void Reconnect();
    redisContext* m_conn;
    RedisConnConf m_conf;
};

#endif







