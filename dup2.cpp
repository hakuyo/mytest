#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char* argv[])  
{  
    fprintf(stderr,"hello error\n");
    int fd = open("hello.txt",O_CREAT|O_RDWR,0666);
    if (fd < 0)
    {
            printf("open error:%d\n",fd);
            return -1;
    }
    printf("close fd %d\n",fd);
    close(fd);
    printf("dup2 2\n");
    dup2(2,fd);
    printf("close 2\n");
    close(2);
    char* buf = "我又可以显示了\n";
    write(fd,buf,strlen(buf));
    fprintf(stderr,"这里显示不了\n");
    return 0;  
}