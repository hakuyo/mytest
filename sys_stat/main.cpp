#include <iostream>
#include "sys_stat.h"
#include <errno.h>
#include <sys/epoll.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <string.h>
#include <stdio.h>

using namespace std;

class A
{
public:
    virtual void onStart()
    {
        cout<<"A onstart"<<endl;
    }
    virtual void onFinish()
    {
        cout<<"A onfinish"<<endl;
    }
};

class B: public A
{
public:
    virtual void onStart()
    {
        cout<<"B onstart"<<endl;
    }
    virtual void onFinish()
    {
        cout<<"B onfinish"<<endl;
    }
};

void test(A& a)
{
    a.onStart();
    a.onFinish();
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        cout<<"usage:"<<endl;
        cout<<"\t./test mem/disk/cpu/proc/net/conn"<<endl;
        return 0;
    }
    string opt(argv[1]);
    int i = 0;
    LinuxServerState stat;
    while ( i++ < 10 )
    {
        if (opt == "cpu")
        {
            cout << "cpu_use:"<<stat.GetCpuUsage()<<endl;
        }
        else if (opt == "mem")
        {
            SysMemInfo meminfo;
            stat.GetMemInfo(meminfo);
            cout<<"meminfo:"<<endl;
            cout<<"\ttotal:"<<meminfo.total<<"M"<<endl;
            cout<<"\tfree:"<<meminfo.free<<"M"<<endl;
            cout<<"\t"<<(meminfo.total - meminfo.free)/meminfo.total<<endl;
        }
        else if (opt == "disk")
        {
            vector<SysDiskInfo> vdiskinfo;
            unsigned long disktotal,diskavail;
            stat.GetDiskInfo(vdiskinfo,disktotal,diskavail);
            cout<<"diskinfo:"<<endl;
            cout<<"\ttotal:"<<disktotal<<"M"<<endl;
            cout<<"\tfree:"<<diskavail<<"M"<<endl;
            cout<<"\tusage:"<<((double)disktotal - diskavail)/disktotal<<endl;
            vector<SysDiskInfo>::iterator itdisk = vdiskinfo.begin();

            cout<<endl;
            for(; itdisk != vdiskinfo.end(); ++itdisk)
            {
                cout<<"\tname:"<<itdisk->name<<endl;
                cout<<"\ttotal:"<<itdisk->total<<"M"<<endl;
                cout<<"\tfree:"<<itdisk->free<<"M"<<endl;
                cout<<endl;
            }
        }
        else if (opt == "net")
        {
            SysNetInfo  netinfo = stat.GetNetInfo();
            cout<<"netinfo:"<<endl;
            cout<<"\trecv:"<<netinfo.recv<<"kb"<<endl;
            cout<<"\tsend:"<<netinfo.send<<"kb"<<endl;
            cout<<"\ttotal:"<<netinfo.total<<"kb"<<endl;
        }
        else if (opt == "proc")
        {
            vector<SysProcInfo> procinfo;
            stat.GetProcInfo(procinfo);
            cout<<"procinfo:"<<endl;
            vector<SysProcInfo>::iterator itproc = procinfo.begin();
            for(; itproc != procinfo.end(); ++itproc)
            {
                cout<<"\tname:"<<itproc->name<<endl;
                cout<<"\tpid:"<<itproc->pid<<endl;
                cout<<"\tmem:"<<itproc->mem<<"Kb"<<endl;
                cout<<"\tcpu:"<<itproc->cpu<<endl;
                cout<<endl;
            }
        }
        else if (opt == "conn")
        {
            vector<SysNetConnInfo> netConnInfo;
            stat.GetNetConnectionInfo(netConnInfo);
            vector<SysNetConnInfo>::iterator itconn = netConnInfo.begin();
            for(; itconn!= netConnInfo.end(); ++itconn)
            {
                cout<<"\tid:"<<itconn->id<<endl;
                cout<<"\tprotocol:"<<itconn->protocol<<endl;
                cout<<"\tlocalAddr:"<<itconn->localAddr<<":"<<itconn->localPort<<endl;
                cout<<"\tremoteAddr:"<<itconn->remoteAddr<<":"<<itconn->remotePort<<endl;
                cout<<"\tstate:"<<itconn->state<<endl;
                cout<<endl;
            }
        }
        sleep(5);
    }
    sleep(1);
    return 0;
}
