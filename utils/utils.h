# ifndef UTIL_H
# define UTIL_H

#include <unistd.h> 
#include <string>
#include <vector>
#include <cstring>
#include <limits.h>

#include <setjmp.h>
#include <signal.h>

using std::string;
using std::vector;

typedef void (*SignalHandler)(int sig);

void install_sig_handler(unsigned int sig,SignalHandler callback);


string to_lower(const string& str);
bool IsFastCGI( void );

int stringSplit(const string& str, const string& sep, vector<string> &vec);
void trim(string& str, const string& trimStr = " \r\n\t");

inline string getAbsExePath()
{
    char buf[ PATH_MAX] = {0}; 
    readlink( "/proc/self/exe", buf, PATH_MAX);
    return string(buf);
}

inline string getAbsExeDir()
{
    char buf[ PATH_MAX] = {0}; 
    readlink( "/proc/self/exe", buf, PATH_MAX);
    string path(buf);
    size_t pos = path.rfind("/");
    if (pos != string::npos)
        return path.substr(0,pos + 1);

    return "";
}

inline string int2a(int val)
{
    char buf[64] = {0};
    sprintf(buf,"%d",val);
    return string(buf);
}

/* from,to: 通过 iconv --list来查询 */
void charset_conv(const string& input,string& output,const string& from,const string& to);

/*
GetTime中的fmt的意义,各标志符间可以加其他格式符，比如冒号
year:
%y: without a century (range 00 to 99)
%Y: including the century

month:
%m:00-12

day:
%d:01-31
%e:1-31  10以下的有前导空格

hour:
%H:00-23
%k:0-23

minute:
%M:00-59

second:
%S:00-60
其他格式参见 strftime 的说明
*/
std::string GetTime(const std::string& fmt);


# endif









