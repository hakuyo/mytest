/*
 * =====================================================================================
 *
 *       Filename:  timer.h
 *
 *    Description:  定时器，一个进程可以多个
 *
 *        Version:  1.0
 *        Created:  01/26/2018 11:10:22 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef TIMER_H_INCLUDE
#define TIMER_H_INCLUDE

#include <string>
#include <map>
#include <signal.h>
#include <time.h>

#include "utils.h"

using std::string;
using std::map;

typedef void (*TimerHandler)(int sig, siginfo_t *info, void *ucontext);
// -lrt needed
const unsigned int max_timer_sig = 100;

class CTimer
{
public:
    // 注册timer，true:成功
    static bool regTimer(const string& name,
            unsigned long sec,
            unsigned long nanosec,
            TimerHandler callback);

    static void clearTimers();

    static timer_t* getTimerId(const string& name);
private:
    static void install_sig_handler(unsigned int sig,TimerHandler callback);
    static void sig_block(int sig);
    static void sig_unblock(int sig);
    static bool install_timer_handler(timer_t *ptid,unsigned long sec,
        TimerHandler callback,unsigned long usec);
    // name:id
    static map<string,timer_t*>   m_ids;
    static unsigned int m_sig;
};

#endif






