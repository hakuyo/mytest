#ifndef REGULAR_H
#define REGULAR_H

#include <regex.h>
#include <string>
#include <vector>

using std::string;
using std::vector;

class CReg
{
public:
    CReg();
    ~CReg();

    bool isMatch(const string& pattern,const string& str);
    //bool isMatch(const string& pattern,const string& str,
    //        vector<string>& matchs,unsigned int nums = 1024);
private:

    bool        m_compiled;
    regex_t     m_reg;
};


#endif







