#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/socket.h>
#include <sys/errno.h>
#include <limits.h>
#include <iconv.h>

#include <string>
#include <vector>

#include <sys/types.h>
#include <sys/stat.h>
#include <fts.h>
#include <memory.h>
#include <time.h>
#include <sys/time.h>

#include "utils.h"


using namespace std;

string to_lower(const string& str)
{
    if (str.empty())
        return str;

    unsigned i = 0;
    string tmp(str.length(),'\0');
    while ( i < str.length())
    {
        tmp[i] = char(tolower(str[i]));
        ++i;
    }
    return tmp;
}

bool IsFastCGI()
{
    int ret = getpeername(0, NULL, NULL);
    if(ret==-1 && errno==ENOTCONN)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int stringSplit(const string& str, const string& sep, vector<string> &vec )
{
    if (str.empty())
        return 0;
    
    size_t posBegin = 0;
    size_t posEnd = str.find(sep,posBegin);
    while(posEnd != string::npos)
    {
        vec.push_back(str.substr(posBegin,posEnd-posBegin));
        posBegin = posEnd + sep.length();
        posEnd = str.find(sep,posBegin);
    }   
    if (posBegin < str.length() )
        vec.push_back(str.substr(posBegin));
    else if (posBegin == str.length() )
        vec.push_back("");

    return vec.size();
}

void trim(string& str, const string& trimStr /* = " \r\n\t" */)
{
    size_t len = str.length();
    size_t begin = 0,end=len;

    while(begin < len)
    {
        if (trimStr.find(str[begin]) == string::npos)
            break;
        ++begin;
    }
    while(end - 1 > 0 && end - 1 > begin)
    {
        if (trimStr.find(str[end - 1]) == string::npos)
            break;
        --end;
    }

    str = str.substr(begin,end - begin);
}


/* from,to: 通过 iconv --list来查询 */
void charset_conv(const string& input,string& output,const string& from,const string& to)
{
    if (input.empty())
        return;

    string tmpInput(input);
    output.clear();
    size_t inLen = tmpInput.length(), outLen = inLen * 4;
    size_t origInLen = inLen, origOutLen = outLen;
    char* pIn = const_cast<char*>(tmpInput.c_str());
    char* pOut = new char[outLen];
    memset(pOut,0,outLen);
    iconv_t dc = iconv_open(to.c_str(),from.c_str());
    /// 转换前
    /// inLen,outLen 必须初始化，分别代表：可从input中读取的最大字节数及 pOut的容量
    /// 转换后
    /// inLen: pIn 剩余字节数
    /// outLen: pOut 中实际有效的字节数
    iconv(dc,&pIn,&inLen,&pOut,&outLen);
    /// 位置有变动
    pIn = pIn - (origInLen - inLen);
    pOut = pOut - (origOutLen - outLen);
    output = string(pOut);
    iconv_close(dc);
    delete[] pOut;
}

string GetTime(const string& fmt)
{
    time_t tmt = time(NULL);
    struct tm *pTime = localtime(&tmt);
    char   timeBuf[64]={0};
    strftime(timeBuf,sizeof(timeBuf),fmt.c_str(),pTime);
    return string(timeBuf);
}

void install_sig_handler(unsigned int sig,SignalHandler callback)
{
	struct sigaction sa;
	sa.sa_handler = callback;
	sa.sa_flags   = 0;
	sigemptyset(&sa.sa_mask);
	sigaction(sig,  &sa, 0);
}















