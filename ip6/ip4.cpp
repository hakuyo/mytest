#include <errno.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <string>

#include "socket_utils.h"
#include <iostream>

using namespace std;

int connect_isgw(const string& ip,unsigned int port)
{
    sockaddr_in saddr;
    memset(&saddr,0,sizeof(saddr));

    saddr.sin_family        = AF_INET;
    saddr.sin_port          = htons(port);
    saddr.sin_addr.s_addr   = inet_addr(ip.c_str());

    int rc = 0;
    int fd = ::socket(AF_INET, SOCK_STREAM, 0);
    if ( fd < 0 )
        return fd;

    do
    {
        rc  = ::connect(fd, (sockaddr*)&saddr, sizeof(saddr));
    }
    while (rc != 0 && errno == EINTR);

    if ( rc != 0 )
    {
        cout<<"errno:"<<errno<<endl;
        cout<<strerror(errno)<<endl;
    }
    return rc == 0?fd:rc;
}

int noblock_send(int fd, const char* buf, int len,unsigned int timeout)
{
    int totalsend = 0;
    int sendlen   = 0;
    struct timeval tstart;
    struct timeval tnow;
    if ( timeout > 0 )
        gettimeofday(&tstart,NULL);
    while(totalsend < len)
    {
        if ( timeout > 0 )
            gettimeofday(&tnow,NULL);

        sendlen = ::send(fd, buf + totalsend,len - totalsend, 0);
        if ( sendlen < 0 )
        {
            if ( timeout > 0 )
            {
                unsigned int msec = 1000*(tnow.tv_sec - tstart.tv_sec) + (tnow.tv_usec - tstart.tv_usec)/1000;
                if ( msec >= timeout )
                    break;
                continue;
            }

            if ( errno == EINTR)
                continue;
            else
                break;
        }
        if ( timeout > 0 )
            tstart = tnow;
        totalsend   += sendlen;
    }

    return totalsend;
}

// 接收完成与否由业务来校验
int noblock_recv(int fd,char* buf,int len, unsigned int timeout)
{
    int total = 0;
    int recvlen = 0;
    struct timeval tstart;
    struct timeval tnow;
    if ( timeout > 0 )
        gettimeofday(&tstart,NULL);
    while( total < len )
    {
        if ( timeout > 0 )
            gettimeofday(&tnow,NULL);

        recvlen = ::recv(fd,buf + total, len - total, 0);
        if ( recvlen < 0)
        {
            if ( timeout > 0 )
            {
                unsigned int msec = 1000*(tnow.tv_sec - tstart.tv_sec) + (tnow.tv_usec - tstart.tv_usec)/1000;
                if ( msec >= timeout )
                    break;
                continue;
            }

            if ( errno == EINTR)
                continue;
            else
                break;
        }
        else if (recvlen == 0)
            break;

        if ( timeout > 0 )
            tstart = tnow;
        total += recvlen;
    }

    return total;
}










