#include "socket_utils.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

using namespace std;

int main(int argc, char* argv[])
{
    if ( argc < 4 )
    {
        cout<<"usage:"<<endl;
        cout<<"\t./test ip port url"<<endl;
        return 0;
    }

    string ip       = string(argv[1]);
    unsigned int port = atoi(argv[2]);
    string url      = string(argv[3]);

    int fd = connect_isgw(ip,port);
    if ( fd < 0)
    {
        cout<<"connect to:"<<ip<<":"<<port<<" failed"<<endl;
        return 0;
    }

    string http_head("GET ");
    http_head += url + " http/1.1\r\n";
    http_head += "Connection: keep-alive\r\n";
    http_head += "Cache-Control: max-age=0\r\n";
    http_head += "Upgrade-Insecure-Requests: 1\r\n";
    http_head += "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36\r\n";
    http_head += "Accept: text/html,application/xhtml+xml,application/xml\r\n";
    http_head += "Accept-Encoding: gzip, deflate, sdch\r\n";
    http_head += "\r\n";

    int len     = noblock_send(fd,http_head.c_str(),http_head.length());
    char resp[2048] = {0};
    len         = noblock_recv(fd,resp,2048,1000);

    cout<<"resp len:"<<len<<endl;
    cout<<resp<<endl;
    close(fd);
    return 0;
}







