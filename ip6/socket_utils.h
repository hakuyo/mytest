#ifndef SOCKET_UTILS_INCLUDED
#define SOCKET_UTILS_INCLUDED

#include <string>

using std::string;

int connect_isgw(const string& ip,unsigned int port);

int noblock_send(int fd, const char* buf, int len,unsigned int timeout = 0);

int noblock_recv(int fd,char* buf,int len, unsigned int timeout = 0);

#endif







