hello=hello
ifndef hi
# 在第一目标前，不能运行外部命令，即不能使用echo
# 可以运行内置命令，但不能以tab开头
# 在目标内，运行命令，只能以tab开关，不管在目标内 使不使用条件运算符
$(warning "hi not defined")
$(error "hi not defined 22222222222")
endif

.PONEY: all all2

all:
ifdef hi
#下面一句必须以tab开头
	@echo "hi not 2"
	@exit 1
endif
	@echo ${hello}
	echo ${hi}

all2:
	@echo "sssss:" ${hello}
	echo "ssssss:" ${hi}
help:
	@echo -e "本Makefile用于测试在命令行给make 指令传参数:\n\
	make hi=1 hi在make文件中就等于1\n\
	hi=1 make hi在make文件中等于1\n\
	hello=2 hi=1 make 在文件中，hello仍为hello,hi=1\n\
	make hello=2 hi=1 在文件中，hello=2,hi=1\n\
	根据以上测试结果，在make前后传递参数的优先级应该是不一样的。"