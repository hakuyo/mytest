/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  测试主程序
 *
 *        Version:  1.0
 *        Created:  01/08/2018 10:52:33 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <string>

#include <vector>

#include "utils.h"

using namespace std;

extern void test(const string& server, const vector<string>& file);

void usage(const char* exe)
{
    cout<<"usage: "<<exe << " options " << "argument" << endl;
    cout<< exe << " -s http-server -f filename"<<endl;
    cout<<"-s       :set trade-service ip:port"<<endl;
    cout<<"-f       :set filename to deal with"<<endl;
    cout<<"filename :if filename is a dir,test all the cases in that dir"<<endl;
    cout<<"          if filename is a file,test the cases in that file"<<endl;
}

int main(int argc, char* argv[])
{
    if (argc > 1)
    {
        string help(argv[1]);
        if (help == "-h"
            || help == "-H"
            || help == "--help"
            || help == "-help"
            || help == "?"
            || help == "-?")
        {
            usage(argv[0]);
            return 0;
        }
    }
    else if (argc == 1)
    {
        usage(argv[0]);
        return 0;
    }
    
    int opt = 0;
    string server;
    vector<string> files;
    while( (opt = getopt(argc,argv,"s:S:f:F:")) !=-1)
    {
        switch(opt)
        {
            case 'S':
            case 's':
                if (optarg != NULL)
                    server = string(optarg);
                break;
            case 'F':
            case 'f':
                if (optarg != NULL)
                    files.push_back(string(optarg));
                break;
            default:
                break;
        }
    }

    test(server,files);

    return 0;
}

