#!/bin/sh

#如果lcov没有装，要先装lcov
#可以从 https://github.com/linux-test-project/lcov.git 上装
#使用前注意事项
#1，启用gcov相关编译选项，见make gcov=1
#2，需要在程序运行并正常退出后，运行此脚本，生成相关覆盖率文件

echo "check lcov cmd"
lcov=$(command -v lcov | wc -l)
if [[ ${lcov} == 0 ]]; then
    echo 请先安装 lcov, 安装来源可以参考 https://github.com/linux-test-project/lcov.git
    exit 0
fi

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

echo "creating $SCRIPTPATH/../gcov"
if [[ ! -d $SCRIPTPATH/../gcov ]]; then
    mkdir $SCRIPTPATH/../gcov
fi

echo "creating ../gcov/gcov.info"
lcov -c -o $SCRIPTPATH/../gcov/gcov.info -d $SCRIPTPATH/../src

echo "creating htmls "
if [[ ! -d $SCRIPTPATH/../gcov/html ]]; then
    mkdir $SCRIPTPATH/../gcov/html
fi

genhtml -o $SCRIPTPATH/../gcov/html $SCRIPTPATH/../gcov/gcov.info
