#include <iconv.h>

/* from,to: 通过 iconv --list来查询 */
void charset_conv(const string& input,string& output,const string& from,const string& to) 
{
    string tmpInput(input);
    output.clear();
    size_t inLen = tmpInput.length(), outLen = inLen * 4;
    size_t origInLen = inLen, origOutLen = outLen;
    char* pIn = const_cast<char*>(tmpInput.c_str());
    char* pOut = new char[outLen];
    memset(pOut,0,outLen);
    iconv_t dc = iconv_open(to.c_str(),from.c_str());
    /// 转换前
    /// inLen,outLen 必须初始化，分别代表：可从input中读取的最大字节数及 pOut的容量
    /// 转换后
    /// inLen: pIn 剩余字节数
    /// outLen: pOut 中实际有效的字节数
    iconv(dc,&pIn,&inLen,&pOut,&outLen);
    /// 指针位置有变动
    pIn = pIn - (origInLen - inLen);
    pOut = pOut - (origOutLen - outLen);
    output = string(pOut);
    iconv_close(dc);
    delete[] pOut;
}



