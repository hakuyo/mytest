#include <unistd.h>
#include <string>
#include <iostream>

#include <glob.h>

using namespace std;

int main(int argc, char* argv[])
{
    glob_t globbuf;
    globbuf.gl_offs = 0;
    // glob可直接用，即主要使用他的模式匹配，带exec系列只是直接执行命令，
    // 与模式匹配已经没有关系了
    // glob直接支持glob("./*.cpp",...),所以，gl_offs可以为0，即不用写命令了,

    /*
    执行exec时，
    对globbuf.gp_pathv的赋值必须放到glob之后，
    globbuf.gl_pathv[0] = "ls";
    globbuf.gl_pathv[1] = "-l";
    */
    glob("*.cpp", GLOB_DOOFFS | GLOB_TILDE , NULL, &globbuf);
    glob("../*csc*", GLOB_DOOFFS | GLOB_TILDE | GLOB_ONLYDIR | GLOB_APPEND, NULL, &globbuf);
    cout<<"gl_pathc:"<<globbuf.gl_pathc<<endl;
    for (int i = globbuf.gl_offs; i < globbuf.gl_pathc + globbuf.gl_offs;i++)
    {
        cout<<globbuf.gl_pathv[i]<<endl;
    }
    
    // 模拟ls -l，要先支行glob，以便给globbuf.gl_pathv分配空间，否则coredump
    //globbuf.gl_pathv[0] = "ls";
    //globbuf.gl_pathv[1] = "-l";
    //execvp("ls", &globbuf.gl_pathv[0]);

    globfree(&globbuf);
    return 0;
}
