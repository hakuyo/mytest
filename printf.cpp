#include <stdio.h>

int main(int argc, char *argv[])
{
        float num = 123.44;
        printf("%0.f\n",num);
        float num2 = 123.664;
        printf("%0.f\n",num2);
        float num3 = 1.664;
        printf("%02.f\n",num3);
        float num4 = 123.664;
        printf("%02.f\n",num4);
        char *str1= "0123456789";
        printf("%2s\n",str1);
        printf("%11s\n",str1);
        printf("%2.3s\n",str1);
        return 0;
}

/*
123
124
02
124
0123456789
 0123456789
012
说明%f的域宽无意义。
%0.f的意义相当于int(一个浮点数)，即相当于一个强制转换

超出宽度，按实际输出
*/